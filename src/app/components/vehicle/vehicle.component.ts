import { Component, OnInit, Input } from '@angular/core';

import { Vehicle } from 'src/app/interfaces/vehicle';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.scss'],
})
export class VechileComponent implements OnInit {

  @Input() vehicles: Vehicle[];

  loading: boolean = true;
  slideOpts = {
    loop: true,
    centeredSlides: true,
    centeredSlidesBounds: true,
    slidesPerView: 'auto'
  };

  constructor() {}

  ngOnInit() {
    setTimeout(() => {
      this.loading = false;
    }, 1500);
  }

}

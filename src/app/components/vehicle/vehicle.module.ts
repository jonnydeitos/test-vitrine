import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VechileComponent } from './vehicle.component';

@NgModule({
  imports: [ CommonModule, FormsModule, IonicModule],
  declarations: [VechileComponent],
  exports: [VechileComponent]
})
export class VechileComponentModule {}

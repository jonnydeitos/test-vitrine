import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VechileComponent } from './vehicle.component';

describe('VechileComponent', () => {
  let component: VechileComponent;
  let fixture: ComponentFixture<VechileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VechileComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VechileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

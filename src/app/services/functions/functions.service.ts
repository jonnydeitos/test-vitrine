import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class FunctionsService {

  constructor(
    private toast: ToastController,
    private loadingCtrl: LoadingController,
  ) { }

  message(text, duration=4000, position=null) {
    this.toast.create({
      message: text,
      duration: duration,
      position: position || 'bottom'
    }).then(toast => toast.present())
  }

  async loading(text='Carregando...') {
    const loading = await this.loadingCtrl.create({
      spinner: "bubbles",
      message: text,
    });
    await loading.present();
    return loading;
  }
}

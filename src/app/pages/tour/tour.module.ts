import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TourPage } from './tour.page';
import { VechileComponentModule } from '../../components/vehicle/vehicle.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    VechileComponentModule,
    RouterModule.forChild([
      { path: '', component: TourPage }
    ])
  ],
  declarations: [TourPage]
})
export class TourPageModule {}

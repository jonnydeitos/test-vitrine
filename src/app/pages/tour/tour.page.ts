import { Component } from '@angular/core';

import { Vehicle } from 'src/app/interfaces/vehicle';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-tour',
  templateUrl: 'tour.page.html',
  styleUrls: ['tour.page.scss']
})
export class TourPage {

  vehicles: Vehicle[] = [];

  constructor() {
    this.vehicles = environment.categories.filter(category => category.idTipoCarro == 1)[0].carros;
  }

}

import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SportPage } from './sport.page';
import { VechileComponentModule } from '../../components/vehicle/vehicle.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    VechileComponentModule,
    RouterModule.forChild([
      { path: '', component: SportPage }
    ])
  ],
  declarations: [SportPage]
})
export class SportPageModule {}

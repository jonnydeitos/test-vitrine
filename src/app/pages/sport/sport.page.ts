import { Component } from '@angular/core';

import { Vehicle } from 'src/app/interfaces/vehicle';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sport',
  templateUrl: 'sport.page.html',
  styleUrls: ['sport.page.scss']
})
export class SportPage {

  vehicles: Vehicle[] = [];

  constructor() {
    this.vehicles = environment.categories.filter(category => category.idTipoCarro == 2)[0].carros;
  }

}

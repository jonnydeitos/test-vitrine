import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  { path: 'categoria', component: TabsPage, children: [
    { path: 'passeio', loadChildren: () => import('../tour/tour.module').then(m => m.TourPageModule) },
    { path: 'esportivo', loadChildren: () => import('../sport/sport.module').then(m => m.SportPageModule) },
    { path: '', redirectTo: '/categoria/passeio', pathMatch: 'full' }
  ]},
  { path: '', redirectTo: '/categoria/passeio', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}

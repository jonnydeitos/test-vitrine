import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Category } from 'src/app/interfaces/vehicle';
import { environment } from 'src/environments/environment';

import { StorageService } from 'src/app/services/storage/storage.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  categories: Category[] = environment.categories;

  constructor(
  	private navCtrl: NavController,
    private storage: StorageService,
    private statusBar: StatusBar,
    private platform: Platform,
	) {}

  ionViewDidEnter(){
    if(this.platform.is('cordova')){
      this.statusBar.backgroundColorByHexString('#444444');
      this.statusBar.styleLightContent();
    }
  }

  logout(){
    this.storage.removeUser()
    this.navCtrl.navigateRoot('/login')
  }

}

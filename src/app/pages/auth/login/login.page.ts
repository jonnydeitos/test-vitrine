import { Component } from '@angular/core';
import { Platform, NavController, MenuController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StatusBar } from '@ionic-native/status-bar/ngx';


import { environment } from 'src/environments/environment';
import { StorageService } from 'src/app/services/storage/storage.service';
import { FunctionsService } from 'src/app/services/functions/functions.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  form: FormGroup;

  constructor(
    private navCtrl: NavController,
    private storage: StorageService,
    private formBuilder: FormBuilder,
    private menuCtrl: MenuController,
    private functions: FunctionsService,
    private statusBar: StatusBar,
    private platform: Platform,
  ) { 
    this.menuCtrl.enable(false);
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ionViewDidEnter(){
    if(this.platform.is('cordova')){
      this.statusBar.backgroundColorByHexString('#FFFFFF');
      this.statusBar.styleDefault();
    }
  }

  async auth(){
    if(this.form.valid){
      const loader = await this.functions.loading('Entrando...');
      setTimeout(() => {
        const data = this.form.value;
        if(environment.user.username == data.username && environment.user.password == data.password){
          this.storage.setUser({token: environment.user.token});
          this.navCtrl.navigateRoot('/categoria/passeio');
        }else{
          this.functions.message('Usuário e senha inválido!');
        }
        loader.dismiss();
      }, 2000);
    }else{
      this.functions.message('Verifique os dados antes de prosseguir!');
    }
  }
}

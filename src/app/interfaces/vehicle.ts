export interface Vehicle {
    idCarro: number;
    nomeCarro: string;
    urlIimage: string;
}

export interface Category {
    idTipoCarro: number;
    tipoCarro: string;
    carros: Vehicle[]
}
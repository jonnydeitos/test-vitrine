// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  user: {username: 'test@test.com', password: 'test@123', token: 'Az7w0xCrzn5KHxNbYWzz81SibbIZCAeDzbzoO1XDeSOmFcEdy4RxulUMCelFfFIU'},
  categories: [
    {
      idTipoCarro: 1,
      tipoCarro: "Passeio ",
      carros: [
        {
          idCarro: 1,
          nomeCarro: "Sandeiro",
          urlIimage: "http://img.olx.com.br/images/34/349803006655584.jpg"
        },
        {
          idCarro: 2,
          nomeCarro: "HB 20",
          urlIimage: "http://img.olx.com.br/images/96/962726108821625.jpg"
        },
        {
          idCarro: 3,
          nomeCarro: "HRV",
          urlIimage: "http://img.olx.com.br/images/50/505711098784658.jpg"
        }
      ]
    },
    {
      idTipoCarro: 2,
      tipoCarro: "Esportivo ",
      carros: [
        {
          idCarro: 1,
          nomeCarro: "Camaro",
          urlIimage: "http://img.olx.com.br/images/88/889709036007504.jpg"
        },
        {
          idCarro: 2,
          nomeCarro: "Ferrari",
          urlIimage: "http://img.olx.com.br/images/27/277727118650506.jpg"
        },
        {
          idCarro: 3,
          nomeCarro: "IX 35",
          urlIimage: "http://img.olx.com.br/images/09/096709117628115.jpg"
        }
      ]
    }
  ]
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
